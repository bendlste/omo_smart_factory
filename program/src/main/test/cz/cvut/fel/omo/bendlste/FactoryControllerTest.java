package cz.cvut.fel.omo.bendlste;

import org.junit.Before;
import org.junit.Test;

import static org.junit.Assert.*;

public class FactoryControllerTest {

    private FactoryController factoryController;
    @Before
    public void setUp() {
        factoryController = new FactoryController();
    }

    @Test
    public void testAddResources_positiveNumber() {
        // arrange
        int expected = 1000;
        // act
        factoryController.addResources(expected);
        // assert
        assertEquals(expected, factoryController.getResources());
    }

    @Test(expected = IllegalArgumentException.class)
    public void testAddResources_negativeNumber() {
        // arrange
        int expected = -1000;
        // act
        factoryController.addResources(expected);
    }

    @Test
    public void testUseResources_positiveNumber() {
        factoryController.addResources(1000);
        int expected = 300;

        factoryController.useResources(700);

        assertEquals(expected, factoryController.getResources());
    }

    @Test(expected = IllegalArgumentException.class)
    public void testUseResources_negativeNumber() {
        factoryController.addResources(1000);

        factoryController.useResources(-100);
    }

    @Test(expected = IllegalStateException.class)
    public void testUseResources_notEnoughResources() {
        factoryController.addResources(1000);

        factoryController.useResources(1001);
    }

}