package cz.cvut.fel.omo.bendlste.Storages;

import cz.cvut.fel.omo.bendlste.FactoryController;
import cz.cvut.fel.omo.bendlste.Machines.Cutter;
import cz.cvut.fel.omo.bendlste.Machines.Dispenser;
import cz.cvut.fel.omo.bendlste.Machines.Filler;
import cz.cvut.fel.omo.bendlste.Machines.Machine;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;
import static org.junit.Assert.assertTrue;

public class MachineStorageTest {

    private FactoryController controller = new FactoryController();
    private MachineStorage machineStorage;

    @Before
    public void setUp() throws Exception {
        machineStorage = new MachineStorage(controller);
        controller.addResources(10000);
    }

    @Test
    public void useMachine() {
        machineStorage.buyCutter();

        assertTrue(machineStorage.hasMachineOfType(Cutter.class));

        machineStorage.useMachine(Cutter.class);

        assertTrue(!machineStorage.hasMachineOfType(Cutter.class));
    }

    @Test(expected = IllegalArgumentException.class)
    public void useMachine_machineNotAvailable() {
        machineStorage.buyCutter();

        assertTrue(machineStorage.hasMachineOfType(Cutter.class));

        machineStorage.useMachine(Dispenser.class);
    }

    @Test
    public void returnMachineToStorage() {
        machineStorage.buyFiller();
        Machine machine = machineStorage.useMachine(Filler.class);

        assertTrue(!machineStorage.hasMachineOfType(Filler.class));

        machineStorage.returnMachineToStorage(machine);

        assertTrue(machineStorage.hasMachineOfType(Filler.class));
    }
}