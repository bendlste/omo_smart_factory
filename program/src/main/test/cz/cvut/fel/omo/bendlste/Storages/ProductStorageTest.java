package cz.cvut.fel.omo.bendlste.Storages;

import cz.cvut.fel.omo.bendlste.FactoryController;
import cz.cvut.fel.omo.bendlste.Product;
import cz.cvut.fel.omo.bendlste.Utils.ProductType;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;

import static org.junit.Assert.*;

public class ProductStorageTest {

    private static FactoryController controller;
    private ProductStorage storage;

    @BeforeClass
    public static void beforeClass() {
        controller = new FactoryController();
        controller.addResources(10000);
    }

    @Before
    public void setUp() {
        this.storage = new ProductStorage(controller);
    }

    @Test
    public void storeProduct() {
        ProductType type = ProductType.BUENO;
        Product product = new Product(type);

        storage.storeProduct(product);

        assertEquals(1, storage.getProductCount(type));
    }

    @Test
    public void storeMultipleProducts() {
        ProductType type1 = ProductType.BUENO;
        ProductType type2 = ProductType.OREO;
        Product product1 = new Product(type1);
        Product product2 = new Product(type2);

        storage.storeProduct(product1);
        storage.storeProduct(product2);

        assertEquals(1, storage.getProductCount(type1));
        assertEquals(1, storage.getProductCount(type2));
    }

    @Test
    public void sellProduct_productStored() {
        ProductType type = ProductType.BUENO;

        storage.storeProduct(new Product(type));

        storage.sellProduct(type, 1);

        assertEquals(0, storage.getProductCount(type));
    }

    @Test
    public void sellMultipleProducts() {
        ProductType type = ProductType.BUENO;

        storage.storeProduct(new Product(type));
        storage.storeProduct(new Product(type));
        storage.storeProduct(new Product(type));

        storage.sellProduct(type, 2);

        assertEquals(1, storage.getProductCount(type));
    }

    @Test(expected = IllegalArgumentException.class)
    public void sellProduct_noSuchProductStored() {
        ProductType type = ProductType.BUENO;

        storage.sellProduct(type, 1);
    }

    @Test(expected = IllegalArgumentException.class)
    public void sellProduct_notEnoughProductsOfSuchType() {
        ProductType type = ProductType.BUENO;
        storage.storeProduct(new Product(type));
        storage.storeProduct(new Product(type));

        storage.sellProduct(type, 3);
    }

    @Test
    public void sellEverything() {
        ProductType type1 = ProductType.BUENO;
        ProductType type2 = ProductType.OREO;
        ProductType type3 = ProductType.KITKAT;

        storage.storeProduct(new Product(type1));
        storage.storeProduct(new Product(type1));
        storage.storeProduct(new Product(type2));
        storage.storeProduct(new Product(type2));
        storage.storeProduct(new Product(type3));
        storage.storeProduct(new Product(type3));

        storage.sellEverything();

        for (ProductType productType : ProductType.values()) {
            assertEquals(0, storage.getProductCount(productType));
        }
    }
}