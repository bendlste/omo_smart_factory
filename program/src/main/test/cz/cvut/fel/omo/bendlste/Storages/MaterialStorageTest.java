package cz.cvut.fel.omo.bendlste.Storages;

import cz.cvut.fel.omo.bendlste.FactoryController;
import cz.cvut.fel.omo.bendlste.Utils.MaterialType;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;

import static org.junit.Assert.*;

public class MaterialStorageTest {

    private static FactoryController controller;
    private MaterialStorage storage;

    @BeforeClass
    public static void beforeClass() {
        controller = new FactoryController();
        controller.addResources(10000);
    }

    @Before
    public void setUp() {
        this.storage = new MaterialStorage(controller);
    }

    @Test
    public void buyMaterial_positiveNumber() {
        int expected = 20;
        MaterialType material = MaterialType.EGGS;

        storage.buyMaterial(material, expected);

        assertEquals(expected, storage.getMaterialQuantity(material));
    }

    @Test(expected = IllegalArgumentException.class)
    public void buyMaterial_negativeNumber() {
        int expected = -20;
        MaterialType material = MaterialType.EGGS;

        storage.buyMaterial(material, expected);
    }

    @Test
    public void buyEverything_positiveNumber() {
        int expected = 10;

        storage.buyEverything(expected);

        for (MaterialType materialType : MaterialType.values()) {
            assertEquals(expected, storage.getMaterialQuantity(materialType));
        }
    }

    @Test(expected = IllegalArgumentException.class)
    public void buyEverything_negativeNumber() {
        int expected = -10;

        storage.buyEverything(expected);
    }

    @Test
    public void useMaterial_materialBought() {
        int expected = 5;
        MaterialType type = MaterialType.CHOCOLATE;
        storage.buyMaterial(type, 10);

        storage.useMaterial(type, 5);

        assertEquals(expected, storage.getMaterialQuantity(type));
    }

    @Test(expected = IllegalArgumentException.class)
    public void useMaterial_noSuchMaterialStored() {
        MaterialType type = MaterialType.FLOUR;

        storage.useMaterial(type, 1);
    }

    @Test(expected = IllegalArgumentException.class)
    public void useMaterial_notEnoughMaterial() {
        MaterialType type = MaterialType.MILK;
        storage.buyMaterial(type, 5);

        storage.useMaterial(type, 10);
    }
}