package cz.cvut.fel.omo.bendlste.Events;

import cz.cvut.fel.omo.bendlste.FactoryController;
import cz.cvut.fel.omo.bendlste.Utils.ProductType;

import java.util.Random;

import static cz.cvut.fel.omo.bendlste.Utils.ProductType.*;

public class ChangeProductEvent extends Event {
    ProductType changeTo;
    ProductType changeFrom;

    public ChangeProductEvent(FactoryController controller, ProductType from) {
        super(controller);
        this.changeFrom = from;
        randomSelect();
    }

    public ProductType getChangeTo() {
        return changeTo;
    }

    public ProductType getChangeFrom() {
        return changeFrom;
    }

    private void randomSelect() {
        Random rn = new Random();
        switch (rn.nextInt(5)){
            case 0:
                changeTo = OREO;
                break;
            case 1:
                changeTo = BEBE;
                break;
            case 2:
                changeTo = KITKAT;
                break;
            case 3:
                changeTo = TWIX;
                break;
            case 4:
                changeTo = BUENO;
                break;
        }
    }

    public void invert() {

    }
}
