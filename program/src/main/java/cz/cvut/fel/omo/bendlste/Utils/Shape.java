package cz.cvut.fel.omo.bendlste.Utils;

public enum Shape {
    OVAL,
    RECTANGLE,
    CIRCLE,
    TRIANGLE,
    SQUARE
}
