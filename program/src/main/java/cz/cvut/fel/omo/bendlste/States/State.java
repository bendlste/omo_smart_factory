package cz.cvut.fel.omo.bendlste.States;

import cz.cvut.fel.omo.bendlste.Machines.Machine;

public interface State {
    /**
     * Changes machine from working to broken and from broken to working.
     *
     * @param machine machine to change state on
     */
    void changeToNextState(Machine machine);
}
