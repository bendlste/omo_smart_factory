package cz.cvut.fel.omo.bendlste.Storages;

import cz.cvut.fel.omo.bendlste.FactoryController;
import cz.cvut.fel.omo.bendlste.Machines.*;
import cz.cvut.fel.omo.bendlste.States.Working;

import java.util.ArrayList;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;

import static cz.cvut.fel.omo.bendlste.Utils.Utils.*;

public class MachineStorage {
    private FactoryController controller;
    private List<Machine> machineStorage;
    private final static Logger LOGGER = Logger.getLogger(MachineStorage.class.getName());
    private int machineCount;

    public MachineStorage(FactoryController controller){
        this.controller = controller;
        machineStorage = new ArrayList();
        machineCount = 0;
    }

    /**
     * Adds cutter to the machine storage.
     * Spends resources of factory.
     */
    public void buyCutter() {
        controller.useResources(CUTTER_COST);
        LOGGER.log(Level.INFO, "Cutter with id: " + (machineCount + 1) + " bought.");
        Machine machine = new Cutter(++machineCount, controller);
        machineStorage.add(machine);
    }

    /**
     * Adds decorator to the machine storage.
     * Spends resources of factory.
     */
    public void buyDecorator(){
        controller.useResources(DECORATOR_COST);
        LOGGER.log(Level.INFO, "Decorator with id: " + (machineCount + 1) + " bought.");
        Machine machine = new Decorator(++machineCount, controller);
        machineStorage.add(machine);
    }

    /**
     * Adds dispenser to the machine storage.
     * Spends resources of factory.
     */
    public void buyDispenser(){
        controller.useResources(DISPENSER_COST);
        LOGGER.log(Level.INFO, "Dispenser with id: " + (machineCount + 1) + " bought.");
        Machine machine = new Dispenser(++machineCount, controller);
        machineStorage.add(machine);
    }

    /**
     * Adds filler to the machine storage.
     * Spends resources of factory.
     */
    public void buyFiller(){
        controller.useResources(FILLER_COST);
        LOGGER.log(Level.INFO, "Filler with id: " + (machineCount + 1) + " bought.");
        Machine machine = new Filler(++machineCount, controller);
        machineStorage.add(machine);
    }

    /**
     * Adds glazer to the machine storage.
     * Spends resources of factory.
     */
    public void buyGlazer(){
        controller.useResources(GLAZER_COST);
        LOGGER.log(Level.INFO, "Glazer with id: " + (machineCount + 1) + " bought.");
        Machine machine = new Glazer(++machineCount, controller);
        machineStorage.add(machine);
    }

    /**
     * Adds kneader to the machine storage.
     * Spends resources of factory.
     */
    public void buyKneader(){
        controller.useResources(KNEADER_COST);
        LOGGER.log(Level.INFO, "Kneader with id: " + (machineCount + 1) + " bought.");
        Machine machine = new Kneader(++machineCount, controller);
        machineStorage.add(machine);
    }

    /**
     * Adds oven to the machine storage.
     * Spends resources of factory.
     */
    public void buyOven(){
        controller.useResources(OVEN_COST);
        LOGGER.log(Level.INFO, "Oven with id: " + (machineCount + 1) + " bought.");
        Machine machine = new Oven(++machineCount, controller);
        machineStorage.add(machine);
    }

    /**
     * Adds packer to the machine storage.
     * Spends resources of factory.
     */
    public void buyPacker(){
        controller.useResources(PACKER_COST);
        LOGGER.log(Level.INFO, "Packer with id: " + (machineCount + 1) + " bought.");
        Robot machine = new PackerRobot(++machineCount, controller);
        machineStorage.add(machine);
    }

    /**
     * Retrieves machine from the storage.
     *
     * @param machineClass class of the machine to retrieve
     * @return retrieved machine
     */
    public Machine useMachine(Class machineClass) {
        for (Machine machine : machineStorage) {
            if (machine.getClass() == machineClass && machine.getState().getClass() == Working.class) {
                machineStorage.remove(machine);
                return machine;
            }
        }
        throw new IllegalArgumentException("No machines of such type stored");
    }

    /**
     * Checks if machine storage has machine of given type available.
     *
     * @param machineClass
     * @return
     */
    public boolean hasMachineOfType(Class machineClass) {
        for (Machine machine : machineStorage) {
            if (machine.getClass() == machineClass && machine.getState().getClass() == Working.class) {
                return true;
            }
        }
        return false;
    }

    /**
     * Returns machine to storage.
     *
     * @param machine machine to return
     */
    public void returnMachineToStorage(Machine machine) {
        machineStorage.add(machine);
    }
}
