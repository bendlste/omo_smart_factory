package cz.cvut.fel.omo.bendlste.States;

import cz.cvut.fel.omo.bendlste.Machines.Machine;

public class Broken implements State {
    public void changeToNextState(Machine machine) {
        machine.setState(new Working());
    }
}
