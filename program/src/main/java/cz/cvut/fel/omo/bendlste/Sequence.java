package cz.cvut.fel.omo.bendlste;

import cz.cvut.fel.omo.bendlste.Machines.*;
import cz.cvut.fel.omo.bendlste.Storages.MachineStorage;
import cz.cvut.fel.omo.bendlste.Utils.ProductType;

import java.util.ArrayList;
import java.util.List;
import java.util.logging.Logger;

import static java.util.logging.Level.INFO;


public class Sequence{

    private List<Class> currentSequence;
    private ProductType currentProduct;
    private MachineStorage machineStorage;
    private final static Logger LOGGER = Logger.getLogger(FactoryController.class.getSimpleName());

    public Sequence(Line line) {
        currentSequence = new ArrayList();
        machineStorage = line.getFactoryController().getMachineStorage();
    }

    public void changeToOreoSequence(){
        List<Class> sequence = new ArrayList();
        sequence.add(Dispenser.class);
        sequence.add(Kneader.class);
        sequence.add(Oven.class);
        sequence.add(Cutter.class);
        sequence.add(Filler.class);
        sequence.add(Decorator.class);
        sequence.add(PackerRobot.class);
        LOGGER.log(INFO, "Changing line to make Oreo");
        currentSequence = sequence;
        currentProduct = ProductType.OREO;
    }

    public void changeToBebeSequence() {
        List<Class> sequence = new ArrayList();
        sequence.add(Dispenser.class);
        sequence.add(Kneader.class);
        sequence.add(Oven.class);
        sequence.add(Cutter.class);
        sequence.add(Decorator.class);
        sequence.add(PackerRobot.class);
        LOGGER.log(INFO, "Changing line to make Bebe");
        currentSequence = sequence;
        currentProduct = ProductType.BEBE;
    }
    
    public void changeToBuenoSequence() {
        List<Class> sequence = new ArrayList();
        sequence.add(Dispenser.class);
        sequence.add(Kneader.class);
        sequence.add(Oven.class);
        sequence.add(Cutter.class);
        sequence.add(Filler.class);
        sequence.add(Glazer.class);
        sequence.add(Decorator.class);
        sequence.add(PackerRobot.class);
        LOGGER.log(INFO, "Changing line to make Bueno");
        currentSequence = sequence;
        currentProduct = ProductType.BUENO;
    }

    public void changeToTwixSequence() {
        List<Class> sequence = new ArrayList();
        sequence.add(Dispenser.class);
        sequence.add(Kneader.class);
        sequence.add(Oven.class);
        sequence.add(Cutter.class);
        sequence.add(Filler.class);
        sequence.add(Glazer.class);
        sequence.add(Decorator.class);
        sequence.add(PackerRobot.class);
        LOGGER.log(INFO, "Changing line to make Twix");
        currentSequence = sequence;
        currentProduct = ProductType.TWIX;
    }
    
    public void changeToKitkatSequence() {
        List<Class> sequence = new ArrayList();
        sequence.add(Dispenser.class);
        sequence.add(Kneader.class);
        sequence.add(Oven.class);
        sequence.add(Cutter.class);
        sequence.add(Filler.class);
        sequence.add(Glazer.class);
        sequence.add(Decorator.class);
        sequence.add(PackerRobot.class);
        LOGGER.log(INFO, "Changing line to make Kitkat");
        currentSequence = sequence;
        currentProduct = ProductType.KITKAT;
    }
    
    public List<Class> getCurrentSequence() {
        return currentSequence;
    }

    public ProductType getCurrentProduct() {
        return currentProduct;
    }

}
