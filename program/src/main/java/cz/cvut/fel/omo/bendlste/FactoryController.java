package cz.cvut.fel.omo.bendlste;

import cz.cvut.fel.omo.bendlste.Events.ChangeProductEvent;
import cz.cvut.fel.omo.bendlste.Events.Event;
import cz.cvut.fel.omo.bendlste.Events.EventStorage;
import cz.cvut.fel.omo.bendlste.Events.TickEvent;
import cz.cvut.fel.omo.bendlste.Humans.Director;
import cz.cvut.fel.omo.bendlste.Humans.Inspector;
import cz.cvut.fel.omo.bendlste.Humans.RepairmenPool;
import cz.cvut.fel.omo.bendlste.Storages.MachineStorage;
import cz.cvut.fel.omo.bendlste.Storages.MaterialStorage;
import cz.cvut.fel.omo.bendlste.Storages.ProductStorage;

import java.util.ArrayList;
import java.util.LinkedList;
import java.util.List;
import java.util.Queue;
import java.util.logging.Level;
import java.util.logging.Logger;

import static cz.cvut.fel.omo.bendlste.Utils.Utils.INITIAL_TICK_SPEED;

public class FactoryController implements Visitable{
    private Director director;
    private Inspector inspector;
    private MachineStorage machineStorage;
    private ProductStorage productStorage;
    private MaterialStorage materialStorage;
    private RepairmenPool repairmenPool;
    private List<Visitable> visitables;
    private List<Observer> observers;
    private Queue<Event> eventQueue;
    private EventStorage eventStorage;
    private int resources;
    private int tick;
    private final static Logger LOGGER = Logger.getLogger(FactoryController.class.getSimpleName());
    private boolean independentMode;

    public FactoryController() {
        this.resources = 0;
        this.tick = 0;
        this.director = Director.getInstance();
        this.inspector = new Inspector();
        this.machineStorage = new MachineStorage(this);
        this.productStorage = new ProductStorage(this);
        this.materialStorage = new MaterialStorage(this);
        this.eventStorage = new EventStorage();
        this.visitables = new ArrayList<Visitable>();
        this.observers = new ArrayList<Observer>();
        this.eventQueue = new LinkedList<Event>();
        this.repairmenPool = new RepairmenPool(this);
        attach(repairmenPool);
    }

    /**
     * Runs factory for N ticks.
     *
     * @param ticksToRun number of ticks to run before the factory takes another commands
     */
    public void startFactory(int ticksToRun) {
        independentMode = false;
        for (int i = 0; i < ticksToRun; i++) {
            tick();
            try {
                Thread.sleep(INITIAL_TICK_SPEED);
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
        }
    }

    /**
     * Starts factory, no further commands allowed, the factory will take care of generating events,
     * buying material and selling products.
     */
    public void startIndependentFactory() {
        independentMode = true;
        while (true) {
            tick();
            try {
                Thread.sleep(INITIAL_TICK_SPEED);
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
        }
    }

    /**
     * Adds funds to the factory, so that it is possible to buy stuff.
     * @param resourcesToAdd
     */
    public void addResources(int resourcesToAdd) {
        if (resourcesToAdd < 0) {
            throw new IllegalArgumentException("Adding resources failed.");
        }
        resources += resourcesToAdd;
        LOGGER.log(Level.INFO,"Added: " + resourcesToAdd + ". Actual balance: " + resources);
    }

    /**
     * Removes resources from the factory.
     * @param resourcesToUse
     */
    public void useResources(int resourcesToUse) {
        if (resourcesToUse < 0)
            throw new IllegalArgumentException("Using resources failed.");
        if (resources - resourcesToUse < 0) {
            throw new IllegalStateException("Not enough resources");
        }
        resources -= resourcesToUse;
        LOGGER.log(Level.INFO,"Spent: " + resourcesToUse + ". Actual balance: " + resources);
    }

    private void processEvents(){
        for (Event event : eventQueue) {
            eventStorage.storeEvent(event);
        }
        notifyObservers();
    }

    /**
     * Adds a new line to the factory.
     * @param line
     */
    public void addLine(Line line) {
        attach(line);
        visitables.add(line);
    }

    /**
     * Removes existing line form the factory.
     * @param line
     */
    public void removeLine(Line line) {
        if (observers.contains(line) && visitables.contains(line)) {
            detach(line);
            visitables.remove(line);
        }
    }

    private void attach(Observer observer) {
        observers.add(observer);
    }

    private void detach(Observer observer) {
        observers.remove(observer);
    }

    public void addEvent(Event event) {
        eventQueue.add(event);
    }

    public void notifyObservers() {
        for (Event event : getEventsFromQueue()) {
            for (Observer observer : observers) {
                observer.update(event);
            }
        }
    }

    public void accept(Director director) {
        for (Visitable visitable : visitables) {
            visitable.accept(director);
        }
    }

    public void accept(Inspector inspector) {
        for (Visitable visitable : visitables) {
            visitable.accept(director);
        }
    }

    /**
     * Performs one tick in the factory.
     */
    public void tick(){
        if (independentMode) doIndependentActions();
        tick++;
        eventQueue.add(new TickEvent(this));
        processEvents();
    }

    /**
     * Performs director visit on this factory.
     */
    public void performDirectorVisitOnFactory() {
        director.visitFactory(this);
    }

    /**
     * Performs director visit on certain line.
     *
     * @param line line to visit
     */
    public void performDirectorVisitOnLine(Line line) {
        director.visitLine(line);
    }

    /**
     * Performs director visit on certain worker.
     *
     * @param worker worker to visit
     */
    public void performDirectorVisitOnWorker(Worker worker) {
        director.visitWorker(worker);
    }

    /**
     * Performs inspector visit on this factory.
     */
    public void performInspectorVisitOnFactory() {
        inspector.visitFactory(this);
    }

    /**
     * Performs inspector visit on certain line.
     *
     * @param line line to visit
     */
    public void performInspectorVisitOnLine(Line line) {
        inspector.visitLine(line);
    }

    /**
     * Performs inspector visit on certain worker.
     *
     * @param worker worker to be visited
     */
    public void performInspectorVisitOnWorker(Worker worker) {
        inspector.visitWorker(worker);
    }

    public int getTick() {
        return tick;
    }

    public MachineStorage getMachineStorage() {
        return machineStorage;
    }

    public ProductStorage getProductStorage() {
        return productStorage;
    }

    public MaterialStorage getMaterialStorage() {
        return materialStorage;
    }

    /**
     * Adds one repairman to the repairmen pool.
     */
    public void hireRepairman() {
        visitables.add(repairmenPool.hireRepairman());
    }

    private List<Event> getEventsFromQueue() {
        List<Event> toRet = new ArrayList<Event>();
        Event event = eventQueue.peek();
        while (event.getTick() <= tick) {
            event = eventQueue.poll();
            toRet.add(event);
            if (eventQueue.isEmpty()) break;
            event = eventQueue.peek();
        }
        return toRet;
    }

    private Line getLine() {
        for (Observer worker : observers) {
            if (worker.getClass() == Line.class) {
                return (Line) worker;
            }
        }
        return null;
    }

    private void doIndependentActions() {
        if (tick % 10 == 0) {
            materialStorage.buyEverything(30);
            productStorage.sellEverything();
        }
        if (tick % 50 == 0 && tick != 0) {
            eventQueue.add(new ChangeProductEvent(this, getLine().getCurrentProduct()));
        }
        if (tick % 75 == 0) {
            director.visitFactory(this);
        }
        if (tick % 200 == 0) {
            inspector.visitFactory(this);
        }
    }

    public int getResources() {
        return resources;
    }
}
