package cz.cvut.fel.omo.bendlste.Machines;

import cz.cvut.fel.omo.bendlste.Events.Event;
import cz.cvut.fel.omo.bendlste.FactoryController;
import cz.cvut.fel.omo.bendlste.Product;

import static cz.cvut.fel.omo.bendlste.Utils.Utils.*;

public class Decorator extends Machine {
    public Decorator(int id, FactoryController controller) {
        super(id,controller);
        controller.useResources(DECORATOR_COST);
    }

    public Product doWork(Product product) {
        switch (product.getType()) {
            case BEBE:
                product.setOrnamentation(BEBE_ORNAMENTATION);
                break;
            case OREO:
                product.setOrnamentation(OREO_ORNAMENTATION);
                break;
            case TWIX:
                product.setOrnamentation(TWIX_ORNAMENTATION);
                break;
            case BUENO:
                product.setOrnamentation(BUENO_ORNAMENTATION);
                break;
            case KITKAT:
                product.setOrnamentation(KITKAT_ORNAMENTATION);
                break;
        }
        return product;
    }

    public void update(Event event) {
        decreaseHealth();
    }

    @Override
    public String toString() {
        return "Decorator";
    }
}
