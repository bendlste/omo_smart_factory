package cz.cvut.fel.omo.bendlste.Storages;

import cz.cvut.fel.omo.bendlste.FactoryController;
import cz.cvut.fel.omo.bendlste.Utils.MaterialType;

import java.util.HashMap;
import java.util.Map;

import static cz.cvut.fel.omo.bendlste.Utils.Utils.*;

public class MaterialStorage {
    private static Map<MaterialType, Integer> material;
    private FactoryController factoryController;

    public MaterialStorage(FactoryController controller) {
        material = new HashMap();
        this.factoryController = controller;
    }

    /**
     * Buys n pieces of every material.
     *
     * @param quantity number of pieces to buy
     */
    public void buyEverything(int quantity) {
        for (MaterialType materialType : MaterialType.values()) {
            buyMaterial(materialType, quantity);
        }
    }

    /**
     * Buys n pieces of material of given type.
     *
     * @param type material type to buy
     * @param quantity number of pieces to buy
     */
    public void buyMaterial(MaterialType type, int quantity) {
        if(quantity < 1) {
            throw new IllegalArgumentException("Insert valid number.");
        }
        if(material.containsKey(type)) {
            material.put(type, material.get(type) + quantity);
        } else {
            material.put(type, quantity);
        }
        payForMaterial(type, quantity);
    }

    /**
     * Removes n pieces of material of given type from material storage.
     *
     * @param type material type to remove
     * @param quantity number of pieces to remove
     */
    public void useMaterial(MaterialType type, int quantity) {
        if(material.containsKey(type)) {
            if(material.get(type) < quantity) {
                throw new IllegalArgumentException("Not enough material.");
            }
            material.put(type, material.get(type) - quantity);
        } else {
            throw new IllegalArgumentException("No such material stored.");
        }
    }

    /**
     * Checks how much material of given type is stored in the storage.
     *
     * @param type type of the material
     * @return number of stored pieces of the material
     */
    public int getMaterialQuantity(MaterialType type) {
        if (material.containsKey(type)) {
            return material.get(type);
        }
        throw new IllegalArgumentException("No such material stored.");
    }

    private void payForMaterial(MaterialType type, int quantity) {
        switch (type) {
            case CHOCOLATE:
                factoryController.useResources(CHOCOLATE_COST * quantity);
                break;
            case MILK:
                factoryController.useResources(MILK_COST * quantity);
                break;
            case NUTS:
                factoryController.useResources(NUTS_COST * quantity);
                break;
            case FLOUR:
                factoryController.useResources(FLOUR_COST * quantity);
                break;
            case EGGS:
                factoryController.useResources(EGG_COST * quantity);
                break;
            case CARAMEL:
                factoryController.useResources(CARAMEL_COST * quantity);
                break;
            case SUGAR:
                factoryController.useResources(SUGAR_COST * quantity);
                break;
            case YEAST:
                factoryController.useResources(YEAST_COST * quantity);
        }
    }
}
