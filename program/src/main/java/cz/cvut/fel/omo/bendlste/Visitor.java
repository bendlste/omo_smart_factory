package cz.cvut.fel.omo.bendlste;

public interface Visitor {

    void visitFactory(FactoryController factoryController);
    void visitLine(Line line);
    void visitWorker(Worker worker);
}
