package cz.cvut.fel.omo.bendlste.Machines;

import cz.cvut.fel.omo.bendlste.Events.Event;
import cz.cvut.fel.omo.bendlste.FactoryController;
import cz.cvut.fel.omo.bendlste.Product;

import static cz.cvut.fel.omo.bendlste.Utils.MaterialType.CHOCOLATE;
import static cz.cvut.fel.omo.bendlste.Utils.Utils.GLAZER_COST;

public class Glazer extends Machine {
    public Glazer(int id, FactoryController controller) {
        super(id, controller);
        controller.useResources(GLAZER_COST);
    }

    public Product doWork(Product product) {
        switch (product.getType()) {
            case BEBE:
            case OREO:
                break;
            case TWIX:
                product.setTopping(CHOCOLATE);
                line.getMaterialFromStorage(CHOCOLATE, 2);
                break;
            case BUENO:
                product.setTopping(CHOCOLATE);
                line.getMaterialFromStorage(CHOCOLATE, 3);
                break;
            case KITKAT:
                product.setTopping(CHOCOLATE);
                line.getMaterialFromStorage(CHOCOLATE, 4);
                break;
        }
        return product;
    }

    public void update(Event event) {
        decreaseHealth();
    }

    @Override
    public String toString() {
        return "Glazer{}";
    }
}
