package cz.cvut.fel.omo.bendlste.Humans;

import cz.cvut.fel.omo.bendlste.FactoryController;
import cz.cvut.fel.omo.bendlste.Line;
import cz.cvut.fel.omo.bendlste.Visitor;
import cz.cvut.fel.omo.bendlste.Worker;

public class Director implements Visitor {
    private static Director instance;

    private Director() {
    }

    public static Director getInstance() {
        if (instance == null) {
            instance = new Director();
        }
        return instance;
    }

    /**
     * Performs a director visit of whole factory.
     *
     * @param factoryController factory controller of the factory to visit
     */
    public void visitFactory(FactoryController factoryController) {
        factoryController.accept(this);
    }

    /**
     * Performs a director visit of certain line.
     *
     * @param line line to visit
     */
    public void visitLine(Line line) {
        line.accept(this);
    }

    /**
     * Performs a director visit of certain worker.
     *
     * @param worker worker to visit
     */
    public void visitWorker(Worker worker) {
        worker.accept(this);
    }

}
