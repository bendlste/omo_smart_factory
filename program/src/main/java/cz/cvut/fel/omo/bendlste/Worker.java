package cz.cvut.fel.omo.bendlste;

import cz.cvut.fel.omo.bendlste.Events.Event;

public abstract class Worker implements Observer, Visitable {
    protected int id;

    public Worker(int id) {
        this.id = id;
    }

    public int getId() {
        return id;
    }

    public abstract void update(Event event);
}
