package cz.cvut.fel.omo.bendlste.Events;

import cz.cvut.fel.omo.bendlste.FactoryController;
import cz.cvut.fel.omo.bendlste.Machines.Machine;

public class BrokenEvent extends Event {
    private Machine brokenMachine;

    public BrokenEvent(FactoryController controller, Machine brokenMachine) {
        super(controller);
        this.brokenMachine = brokenMachine;
    }

    public Machine getBrokenMachine() {
        return brokenMachine;
    }

    public void invert() {

    }
}
