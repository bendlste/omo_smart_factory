package cz.cvut.fel.omo.bendlste.Machines;

import cz.cvut.fel.omo.bendlste.Events.Event;
import cz.cvut.fel.omo.bendlste.FactoryController;
import cz.cvut.fel.omo.bendlste.Product;

import static cz.cvut.fel.omo.bendlste.Utils.Utils.*;

public class Kneader extends Machine {
    public Kneader(int id, FactoryController controller) {
        super(id, controller);
        controller.useResources(KNEADER_COST);
    }

    @Override
    public Product doWork(Product product) {
        switch (product.getType()) {
            case BEBE:
                product.setSmoothness(BEBE_SMOOTHNESS);
                break;
            case OREO:
                product.setSmoothness(OREO_SMOOTHNESS);
                break;
            case TWIX:
                product.setSmoothness(TWIX_SMOOTHNESS);
                break;
            case BUENO:
                product.setSmoothness(BUENO_SMOOTHNESS);
                break;
            case KITKAT:
                product.setSmoothness(KITKAT_SMOOTHNESS);
                break;
        }
        return product;
    }

    public void update(Event event) {
        decreaseHealth();
    }

    @Override
    public String toString() {
        return "Kneader";
    }
}
