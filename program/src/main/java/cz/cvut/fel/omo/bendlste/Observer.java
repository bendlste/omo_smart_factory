package cz.cvut.fel.omo.bendlste;

import cz.cvut.fel.omo.bendlste.Events.Event;

public interface Observer {
    /**
     * Simulates communication in factory.
     *
     * @param event
     */
    void update(Event event);
}
