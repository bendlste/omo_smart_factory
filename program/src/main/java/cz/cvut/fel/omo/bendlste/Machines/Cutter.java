package cz.cvut.fel.omo.bendlste.Machines;

import cz.cvut.fel.omo.bendlste.Events.Event;
import cz.cvut.fel.omo.bendlste.FactoryController;
import cz.cvut.fel.omo.bendlste.Product;

import static cz.cvut.fel.omo.bendlste.Utils.Shape.*;
import static cz.cvut.fel.omo.bendlste.Utils.Utils.*;


public class Cutter extends Machine {
    public Cutter(int id, FactoryController controller) {
        super(id,controller);
        controller.useResources(CUTTER_COST);
    }

    @Override
    public Product doWork(Product product) {
        switch (product.getType()) {
            case BEBE:
                product.setSize(BEBE_SIZE);
                product.setShape(OVAL);
                break;
            case OREO:
                product.setSize(OREO_SIZE);
                product.setShape(CIRCLE);
                break;
            case TWIX:
                product.setSize(TWIX_SIZE);
                product.setShape(RECTANGLE);
                break;
            case BUENO:
                product.setSize(BUENO_SIZE);
                product.setShape(OVAL);
                break;
            case KITKAT:
                product.setSize(KITKAT_SIZE);
                product.setShape(RECTANGLE);
                break;
        }
        return product;
    }

    public void update(Event event) {
        decreaseHealth();
    }

    @Override
    public String toString() {
        return "Cutter";
    }
}
