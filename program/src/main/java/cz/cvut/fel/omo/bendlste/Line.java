package cz.cvut.fel.omo.bendlste;

import cz.cvut.fel.omo.bendlste.Events.*;
import cz.cvut.fel.omo.bendlste.Humans.Director;
import cz.cvut.fel.omo.bendlste.Humans.Inspector;
import cz.cvut.fel.omo.bendlste.Machines.Machine;
import cz.cvut.fel.omo.bendlste.States.Working;
import cz.cvut.fel.omo.bendlste.Storages.MachineStorage;
import cz.cvut.fel.omo.bendlste.Storages.MaterialStorage;
import cz.cvut.fel.omo.bendlste.Storages.ProductStorage;
import cz.cvut.fel.omo.bendlste.Utils.MaterialType;
import cz.cvut.fel.omo.bendlste.Utils.ProductType;

import java.util.ArrayList;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;

public class Line implements Observer, Visitable{
    private List<Machine> machines;
    private Sequence sequence;
    private boolean running;
    private FactoryController factoryController;
    private ProductStorage productStorage;
    private MaterialStorage materialStorage;
    private MachineStorage machineStorage;
    private List<Product> products;
    private int batch;
    private final static Logger LOGGER = Logger.getLogger(Line.class.getSimpleName());

    public Line(FactoryController factoryController) {
        this.machines = new ArrayList<Machine>();
        this.products = new ArrayList<Product>();
        this.factoryController = factoryController;
        this.sequence = new Sequence(this);
        this.running = false;
        this.batch = 0;
        this.productStorage = factoryController.getProductStorage();
        this.materialStorage = factoryController.getMaterialStorage();
        this.machineStorage = factoryController.getMachineStorage();
    }

    /**
     * Starts line, does actions needed to start the line.
     * If some of the machines are missing, aborts starting the line.
     */
    public void startLine() {
        if (running) return;
        assignMachines();
        if (!checkHasNeededMachines()) {
            LOGGER.log(Level.INFO, "Line starting unsuccessful.");
            return;
        }
        running = true;
        LOGGER.log(Level.INFO, "Line started");
    }

    private void pauseLine() {
        if (!running) return;
        running = false;
        LOGGER.log(Level.INFO, "Line paused");
    }

    private void resumeLine() {
        if (running) return;
        if (!checkHasNeededMachines()) {
            LOGGER.log(Level.INFO, "Line resuming unsuccessful");
            return;
        }
        running = true;
        LOGGER.log(Level.INFO, "Line resumed");
    }

    public void stopLine() {
        if (!running) return;
        running = false;
        products.clear();
        LOGGER.log(Level.INFO, "Line stopped");
    }

    /**
     * Change production line to make certain product.
     *
     * @param type type of product
     */
    public void changeProduct(ProductType type) {
        stopLine();
        switch (type) {
            case BEBE:
                makeBebe();
                break;
            case BUENO:
                makeBueno();
                break;
            case TWIX:
                makeTwix();
                break;
            case OREO:
                makeOreo();
                break;
            case KITKAT:
                makeKitkat();
                break;
        }
        batch++;
        startLine();
    }

    /**
     * Retrieves material of given type from storage.
     * @param type
     * @param quantity
     */
    public void getMaterialFromStorage(MaterialType type, int quantity) {
        materialStorage.useMaterial(type, quantity);
    }

    /**
     * Actions performed on every event.
     * @param event
     */
    public void update(Event event) {
        if (event.getClass() == RepairedEvent.class) {
            startLine();
        }
        if (event.getClass() == BrokenEvent.class) {
            pauseLine();
            resumeLine();
        }
        if (event.getClass() == ChangeProductEvent.class) {
            onChangeProductEvent((ChangeProductEvent) event);
        }
        if (!checkHasNeededMachines()) return;
        if (event.getClass() == TickEvent.class) {
            doWork();
            for (Worker worker : machines) {
                worker.update(event);
            }
        }
    }

    /**
     * Accepts director as visitor.
     * @param director
     */
    public void accept(Director director) {
        for (Machine machine : machines) {
            machine.accept(director);
        }
    }

    /**
     * Accepts inspector as visitor.
     * @param inspector
     */
    public void accept(Inspector inspector) {
        for (Machine machine : machines) {
            machine.accept(inspector);
        }
    }

    public FactoryController getFactoryController() {
        return factoryController;
    }

    public ProductType getCurrentProduct() {
        return sequence.getCurrentProduct();
    }

    private void onChangeProductEvent(ChangeProductEvent event) {
        changeProduct(event.getChangeTo());
    }

    private void doWork() {
        if(!running) return;
        checkProduced();
        createNewProduct();
        for (Product product : products) {
            if (machines.get(product.getPositionInLine()) != null) {
                machines.get(product.getPositionInLine()).doWork(product);
                product.setPositionInLine(product.getPositionInLine() + 1);
            }
        }
    }

    private void checkProduced() {
        if(products.isEmpty()) return;
        if(products.get(0).getPositionInLine() == sequence.getCurrentSequence().size() - 1) {
            productStorage.storeProduct(products.get(0));
            products.remove(0);
        }
    }

    private void createNewProduct() {
        Product newProduct = new Product(sequence.getCurrentProduct());
        newProduct.setBatch(batch);
        products.add(newProduct);
    }

    private boolean tryReplaceBrokenMachine(Machine machine) {
        if (machine.getState().getClass() != Working.class) {
            if (checkMachineStorageHasReplacement(machine)) {
                replaceBrokenMachine(machine);
            } else {
                return false;
            }
        }
        return true;
    }

    private boolean checkHasNeededMachines() {
        if (!checkMachinesAreWorking()) return false;

        for (Class machineClass : sequence.getCurrentSequence()) {
            boolean helper = false;
            for (Machine machine : machines) {
                if (machine.getClass() == machineClass)
                    helper = true;
            }
            if (!helper) {
                return false;
            }
        }
        return true;
    }

    private void assignMachines() {
        for (Machine machine : machines) {
            machineStorage.returnMachineToStorage(machine);
            machine.assignToLine(null);
        }
        machines.clear();
        for (Class machineClass : sequence.getCurrentSequence()) {
            if (machineStorage.hasMachineOfType(machineClass)) {
                Machine machine = machineStorage.useMachine(machineClass);
                machine.assignToLine(this);
                machines.add(machine);
            }
        }
    }

    private void makeOreo() {
        sequence.changeToOreoSequence();
    }

    private void makeBebe() {
        sequence.changeToBebeSequence();
    }

    private void makeKitkat() {
        sequence.changeToKitkatSequence();
    }

    private void makeBueno() {
        sequence.changeToBuenoSequence();
    }

    private void makeTwix() {
        sequence.changeToTwixSequence();
    }

    private boolean checkMachinesAreWorking() {
        for (int i = 0; i < machines.size(); i++) {
            Machine machine = machines.get(i);
            if (machine.getState().getClass() != Working.class) {
                if (!tryReplaceBrokenMachine(machine)) {
                    return false;
                }
            }
        }
        return true;
    }

    private boolean checkMachineStorageHasReplacement(Machine machine) {
        return machineStorage.hasMachineOfType(machine.getClass());
    }

    private void replaceBrokenMachine(Machine machine) {
        machineStorage.returnMachineToStorage(machine);
        machines.remove(machine);
        machine.assignToLine(null);
        Machine replacementMachine = machineStorage.useMachine(machine.getClass());
        replacementMachine.assignToLine(this);
        machines.add(replacementMachine);
        LOGGER.log(Level.INFO, "Replaced " + machine + " with " + machine + " from machine storage.");
    }

}
