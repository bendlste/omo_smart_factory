package cz.cvut.fel.omo.bendlste;

import cz.cvut.fel.omo.bendlste.Storages.MachineStorage;
import cz.cvut.fel.omo.bendlste.Storages.MaterialStorage;
import cz.cvut.fel.omo.bendlste.Utils.ProductType;

import static cz.cvut.fel.omo.bendlste.Utils.MaterialType.*;

public class Main {
    public static void main(String[] args) {
        FactoryController controller = new FactoryController();
        controller.addResources(999999999);
        initFactory(controller);

        Line line = new Line(controller);
        controller.addLine(line);
        line.changeProduct(ProductType.OREO);
        controller.startIndependentFactory();
    }

    private static void initFactory(FactoryController controller) {
        MachineStorage machineStorage = controller.getMachineStorage();
        MaterialStorage materialStorage = controller.getMaterialStorage();

        for (int i = 0; i < 2; i++) {
            machineStorage.buyCutter();
            machineStorage.buyDecorator();
            machineStorage.buyDispenser();
            machineStorage.buyFiller();
            machineStorage.buyGlazer();
            machineStorage.buyKneader();
            machineStorage.buyOven();
            machineStorage.buyPacker();
        }

        materialStorage.buyMaterial(CHOCOLATE, 1000);
        materialStorage.buyMaterial(CARAMEL, 1000);
        materialStorage.buyMaterial(YEAST, 1000);
        materialStorage.buyMaterial(FLOUR, 500);
        materialStorage.buyMaterial(MILK, 2500);
        materialStorage.buyMaterial(SUGAR, 500);
        materialStorage.buyMaterial(EGGS, 500);
        materialStorage.buyMaterial(NUTS, 500);

        for (int i = 0; i < 3; i++) {
            controller.hireRepairman();
        }
    }
}
