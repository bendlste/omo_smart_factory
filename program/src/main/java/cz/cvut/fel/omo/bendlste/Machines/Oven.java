package cz.cvut.fel.omo.bendlste.Machines;

import cz.cvut.fel.omo.bendlste.Events.Event;
import cz.cvut.fel.omo.bendlste.FactoryController;
import cz.cvut.fel.omo.bendlste.Product;

import static cz.cvut.fel.omo.bendlste.Utils.Utils.*;

public class Oven extends Machine {
    public Oven(int id, FactoryController controller) {
        super(id, controller);
        controller.useResources(OVEN_COST);
    }

    @Override
    public Product doWork(Product product) {
        switch (product.getType()) {
            case BEBE:
                product.setFragility(BEBE_FRAGILITY);
                break;
            case OREO:
                product.setFragility(OREO_FRAGILITY);
                break;
            case TWIX:
                product.setFragility(TWIX_FRAGILITY);
                break;
            case BUENO:
                product.setFragility(BUENO_FRAGILITY);
                break;
            case KITKAT:
                product.setFragility(KITKAT_FRAGILITY);
                break;
        }
        return product;
    }

    public void update(Event event) {
        decreaseHealth();
    }

    @Override
    public String toString() {
        return "Oven";
    }
}
