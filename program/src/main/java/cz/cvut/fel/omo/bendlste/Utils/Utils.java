package cz.cvut.fel.omo.bendlste.Utils;

public class Utils {
    public static final int BEBE_SIZE = 8;
    public static final int BEBE_ORNAMENTATION = 5;
    public static final int BEBE_SMOOTHNESS = 2;
    public static final int BEBE_FRAGILITY = 5;

    public static final int OREO_SIZE = 6;
    public static final int OREO_ORNAMENTATION = 3;
    public static final int OREO_SMOOTHNESS = 2;
    public static final int OREO_FRAGILITY = 4;

    public static final int TWIX_SIZE = 12;
    public static final int TWIX_ORNAMENTATION = 4;
    public static final int TWIX_SMOOTHNESS = 4;
    public static final int TWIX_FRAGILITY = 1;

    public static final int BUENO_SIZE = 11;
    public static final int BUENO_ORNAMENTATION = 7;
    public static final int BUENO_SMOOTHNESS = 4;
    public static final int BUENO_FRAGILITY = 3;

    public static final int KITKAT_SIZE = 9;
    public static final int KITKAT_ORNAMENTATION = 1;
    public static final int KITKAT_SMOOTHNESS = 5;
    public static final int KITKAT_FRAGILITY = 2;

    public static final int CUTTER_COST = 5000;
    public static final int DECORATOR_COST = 4000;
    public static final int DISPENSER_COST = 6000;
    public static final int FILLER_COST = 4500;
    public static final int GLAZER_COST = 5500;
    public static final int KNEADER_COST = 6500;
    public static final int OVEN_COST = 8000;
    public static final int PACKER_COST = 3500;

    public static final int CHOCOLATE_COST = 2;
    public static final int MILK_COST = 2;
    public static final int CARAMEL_COST = 2;
    public static final int SUGAR_COST = 1;
    public static final int NUTS_COST = 3;
    public static final int YEAST_COST = 1;
    public static final int EGG_COST = 1;
    public static final int FLOUR_COST = 2;

    public static final int MACHINE_COST_PER_TICK = 10;

    public static final int BEBE_PRICE = 7;
    public static final int OREO_PRICE = 10;
    public static final int TWIX_PRICE = 12;
    public static final int BUENO_PRICE = 14;
    public static final int KITKAT_PRICE = 8;

    public static final int INITIAL_REPAIRING_SPEED = 300;
    public static final int INITIAL_MACHINE_HEALTH_DECREASING_SPEED = 40;

    public static final int INITIAL_TICK_SPEED = 100;
}