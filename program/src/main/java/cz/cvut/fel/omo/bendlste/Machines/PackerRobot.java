package cz.cvut.fel.omo.bendlste.Machines;

import cz.cvut.fel.omo.bendlste.Events.Event;
import cz.cvut.fel.omo.bendlste.FactoryController;
import cz.cvut.fel.omo.bendlste.Product;

import static cz.cvut.fel.omo.bendlste.Utils.Utils.PACKER_COST;

public class PackerRobot extends Robot {
    public PackerRobot(int id, FactoryController controller) {
        super(id, controller);
        controller.useResources(PACKER_COST);
    }

    @Override
    public Product doWork(Product product) {
        product.pack();
        return product;
    }

    public void update(Event event) {
        decreaseHealth();
    }

    @Override
    public String toString() {
        return "PackerRobot";
    }
}
