package cz.cvut.fel.omo.bendlste.Events;

import java.util.Stack;

public class EventStorage {
    private Stack<Event> eventList;


    public EventStorage() {
        this.eventList = new Stack<Event>();
    }

    public void storeEvent(Event event) {
        eventList.add(event);
    }
}
