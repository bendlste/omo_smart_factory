package cz.cvut.fel.omo.bendlste.Machines;

import cz.cvut.fel.omo.bendlste.Events.Event;
import cz.cvut.fel.omo.bendlste.FactoryController;
import cz.cvut.fel.omo.bendlste.Product;

import static cz.cvut.fel.omo.bendlste.Utils.MaterialType.*;
import static cz.cvut.fel.omo.bendlste.Utils.Utils.FILLER_COST;

public class Filler extends Machine {
    public Filler(int id, FactoryController controller) {
        super(id, controller);
        controller.useResources(FILLER_COST);
    }

    @Override
    public Product doWork(Product product) {
        switch (product.getType()) {
            case BEBE:
                product.setFilling(NO_MATERIAL);
                break;
            case OREO:
                product.setFilling(MILK);
                line.getMaterialFromStorage(MILK, 1);
                break;
            case TWIX:
                product.setFilling(CARAMEL);
                line.getMaterialFromStorage(CARAMEL, 2);
                break;
            case BUENO:
                product.setFilling(CHOCOLATE);
                line.getMaterialFromStorage(CHOCOLATE, 2);
                break;
            case KITKAT:
                product.setFilling(NUTS);
                line.getMaterialFromStorage(NUTS, 1);
                break;
        }
        return product;
    }

    public void update(Event event) {
        decreaseHealth();
    }

    @Override
    public String toString() {
        return "Filler";
    }
}
