package cz.cvut.fel.omo.bendlste.Humans;

import cz.cvut.fel.omo.bendlste.Events.Event;
import cz.cvut.fel.omo.bendlste.Worker;

public abstract class Human extends Worker {

    public Human(int id) {
        super(id);
    }

    public abstract void update(Event event);

}
