package cz.cvut.fel.omo.bendlste.Machines;

import cz.cvut.fel.omo.bendlste.Events.BrokenEvent;
import cz.cvut.fel.omo.bendlste.Events.Event;
import cz.cvut.fel.omo.bendlste.Events.TickEvent;
import cz.cvut.fel.omo.bendlste.*;
import cz.cvut.fel.omo.bendlste.Humans.Director;
import cz.cvut.fel.omo.bendlste.Humans.Inspector;
import cz.cvut.fel.omo.bendlste.States.State;
import cz.cvut.fel.omo.bendlste.States.Working;

import java.util.Random;
import java.util.logging.Level;
import java.util.logging.Logger;

import static cz.cvut.fel.omo.bendlste.Utils.Utils.INITIAL_MACHINE_HEALTH_DECREASING_SPEED;
import static cz.cvut.fel.omo.bendlste.Utils.Utils.MACHINE_COST_PER_TICK;

public abstract class Machine extends Worker implements Visitable {
    private State state;
    private int health;
    protected Line line;
    private int costPerTick;
    private int healthDecreasePerTick;
    protected FactoryController controller;
    private Random rn = new Random();
    private final static Logger LOGGER = Logger.getLogger(FactoryController.class.getSimpleName());

    public Machine(int id, FactoryController controller) {
        super(id);
        this.state = new Working();
        this.health = 1000;
        this.costPerTick = MACHINE_COST_PER_TICK;
        this.controller = controller;
        this.healthDecreasePerTick = INITIAL_MACHINE_HEALTH_DECREASING_SPEED;
    }

    public void assignToLine(Line line) {
        this.line = line;
    }

    public abstract Product doWork(Product product);

    public void accept(Director director) {
        LOGGER.log(Level.INFO, "Director visited " + this + ".");
        health += 100;
    }

    public void accept(Inspector inspector) {
        LOGGER.log(Level.INFO, "Inspector visited " + this + ".");
        decreaseHealth();
    }

    public void update(Event event) {
        if (event.getClass() == TickEvent.class) {
            controller.useResources(costPerTick);
        }
    }

    public State getState() {
        return state;
    }

    public void changeToNextState() {
        state.changeToNextState(this);
    }

    public void setState(State state) {
        this.state = state;
    }

    public int getHealth() {
        return health;
    }

    public void setHealth(int health) {
        this.health = health;
    }

    public Line getLine() {
        return line;
    }

    /**
     * Simulates machine breaking.
     */
    protected void decreaseHealth() {
        this.setHealth(this.getHealth() - rn.nextInt(healthDecreasePerTick));
        if (this.health <= 0) {
            LOGGER.log(Level.INFO, "Machine " + this + " just broke." );
            controller.addEvent(new BrokenEvent(controller, this));
            state.changeToNextState(this);
        }
    }

}
