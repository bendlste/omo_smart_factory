package cz.cvut.fel.omo.bendlste.Humans;

import cz.cvut.fel.omo.bendlste.Events.BrokenEvent;
import cz.cvut.fel.omo.bendlste.Events.Event;
import cz.cvut.fel.omo.bendlste.Events.TickEvent;
import cz.cvut.fel.omo.bendlste.Machines.Machine;
import cz.cvut.fel.omo.bendlste.Utils.Utils;

import java.util.Random;
import java.util.logging.Level;
import java.util.logging.Logger;

public class Repairman extends Human{
    private RepairmenPool repairmenPool;
    private boolean repairing;
    private Machine machine;
    private int repairingSpeed;
    private Random rn = new Random();
    private final static Logger LOGGER = Logger.getLogger(Repairman.class.getSimpleName());

    public Repairman(int id, RepairmenPool repairmenPool) {
        super(id);
        repairing = false;
        this.repairmenPool = repairmenPool;
        this.repairingSpeed = Utils.INITIAL_REPAIRING_SPEED;
    }

    /**
     * Actions performed on each event.
     *
     * @param event
     */
    public void update(Event event) {
        if (event.getClass() == BrokenEvent.class) {
            onBrokenEvent((BrokenEvent) event);
        } else if (event.getClass() == TickEvent.class) {
            onTickEvent();
        }
    }

    public boolean isRepairing() {
        return repairing;
    }

    private void repair() {
        machine.setHealth(machine.getHealth() + rn.nextInt(repairingSpeed));
        if (machine.getHealth() >= 1000) {
            machine.setHealth(1000);
            machine.changeToNextState();
            generateRepairedEvent();
            this.machine = null;
            repairing = false;
        }
    }

    private void onTickEvent(){
        if (machine != null) {
            repair();
        }
    }

    private void onBrokenEvent(BrokenEvent event) {
        if (machine == null) {
            repairing = true;
            machine = event.getBrokenMachine();
            LOGGER.log(Level.INFO, "Starting repairment on " + machine + ".");
        }
    }

    private void generateRepairedEvent() {
        repairmenPool.generateRepairedEvent(machine);
    }

    public void accept(Director director) {
        LOGGER.log(Level.INFO,"Director visited " + this + ".");
        onTickEvent();
    }

    public void accept(Inspector inspector) {
        LOGGER.log(Level.INFO,"Inspector visited " + this + ".");
        if (machine != null) {
            machine.setHealth(machine.getHealth() - repairingSpeed);
        }
    }

    @Override
    public String toString() {
        return "Repairman{" +
                "id=" + id +
                '}';
    }
}
