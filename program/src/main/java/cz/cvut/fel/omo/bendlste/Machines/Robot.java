package cz.cvut.fel.omo.bendlste.Machines;

import cz.cvut.fel.omo.bendlste.FactoryController;
import cz.cvut.fel.omo.bendlste.Product;

public abstract class Robot extends Machine {
    public Robot(int id, FactoryController controller){ super(id, controller); }

    @Override
    public abstract Product doWork(Product product);

}
