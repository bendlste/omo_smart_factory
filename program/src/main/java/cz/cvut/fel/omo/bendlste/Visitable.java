package cz.cvut.fel.omo.bendlste;

import cz.cvut.fel.omo.bendlste.Humans.Director;
import cz.cvut.fel.omo.bendlste.Humans.Inspector;

public interface Visitable {
    void accept(Director director);
    void accept(Inspector inspector);
}
