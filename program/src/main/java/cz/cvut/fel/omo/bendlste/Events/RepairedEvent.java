package cz.cvut.fel.omo.bendlste.Events;

import cz.cvut.fel.omo.bendlste.FactoryController;
import cz.cvut.fel.omo.bendlste.Machines.Machine;

public class RepairedEvent extends Event {
    private Machine repairedMachine;
    public RepairedEvent(FactoryController controller, Machine repairedMachine) {
        super(controller);
        this.repairedMachine = repairedMachine;
    }

    public void invert() {

    }
}
