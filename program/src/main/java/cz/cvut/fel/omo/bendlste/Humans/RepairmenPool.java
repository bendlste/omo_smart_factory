package cz.cvut.fel.omo.bendlste.Humans;

import cz.cvut.fel.omo.bendlste.Events.BrokenEvent;
import cz.cvut.fel.omo.bendlste.Events.Event;
import cz.cvut.fel.omo.bendlste.Events.RepairedEvent;
import cz.cvut.fel.omo.bendlste.FactoryController;
import cz.cvut.fel.omo.bendlste.Machines.Machine;
import cz.cvut.fel.omo.bendlste.Observer;

import java.util.HashSet;
import java.util.Set;
import java.util.logging.Level;
import java.util.logging.Logger;

public class RepairmenPool implements Observer {
    private Set<Repairman> repairmenPool;
    private FactoryController factoryController;
    private final static Logger LOGGER = Logger.getLogger(FactoryController.class.getSimpleName());

    public RepairmenPool(FactoryController factoryController) {
        this.repairmenPool = new HashSet<Repairman>();
        this.factoryController = factoryController;
    }

    /**
     * Actions performed on each event
     *
     * @param event
     */
    public void update(Event event) {
        if (event.getClass() == BrokenEvent.class) {
            for (Repairman repairman : repairmenPool) {
                if (!repairman.isRepairing()) {
                    repairman.update(event);
                    return;
                }
            }
            // If no repairman is available, return the event to the event queue.
            event.delayToNextTick();
            factoryController.addEvent(event);
        } else {
            for (Repairman repairman : repairmenPool) {
                repairman.update(event);
            }
        }
    }

    /**
     * Adds repaired event to te eventQueue of factory controller.
     *
     * @param machine
     */
    public void generateRepairedEvent(Machine machine) {
        factoryController.addEvent(new RepairedEvent(factoryController, machine));
        LOGGER.log(Level.INFO, "Machine " + machine + " repaired.");
    }

    public Repairman hireRepairman() {
        Repairman repairman = new Repairman(repairmenPool.size(), this);
        repairmenPool.add(repairman);
        return repairman;
    }

    public FactoryController getFactoryController() {
        return factoryController;
    }
}
