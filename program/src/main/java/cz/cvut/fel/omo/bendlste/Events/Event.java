package cz.cvut.fel.omo.bendlste.Events;

import cz.cvut.fel.omo.bendlste.FactoryController;

public abstract class Event {
    int tick;
    FactoryController controller;

    public Event(FactoryController controller) {
        this.controller = controller;
        this.tick = controller.getTick();

    }

    public int getTick() {
        return tick;
    }

    public void delayToNextTick() {
        tick++;
    }

    public abstract void invert();
}
