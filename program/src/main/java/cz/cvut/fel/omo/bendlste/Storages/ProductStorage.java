package cz.cvut.fel.omo.bendlste.Storages;

import cz.cvut.fel.omo.bendlste.FactoryController;
import cz.cvut.fel.omo.bendlste.Product;
import cz.cvut.fel.omo.bendlste.Utils.ProductType;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.logging.Level;
import java.util.logging.Logger;

import static cz.cvut.fel.omo.bendlste.Utils.Utils.*;

public class ProductStorage {
    private FactoryController controller;
    private Map<ProductType, List<Product>> productStorage;
    private final static Logger LOGGER = Logger.getLogger(ProductStorage.class.getSimpleName());

    public ProductStorage(FactoryController controller) {
        productStorage = new HashMap<ProductType, List<Product>>();
        this.controller = controller;
    }

    /**
     * Stores given product to the storage.
     *
     * @param product product to store
     */
    public void storeProduct(Product product) {
        ProductType type = product.getType();
        if(!productStorage.containsKey(type)) {
            productStorage.put(type, new ArrayList<Product>());
        }
        productStorage.get(type).add(product);
        LOGGER.log(Level.INFO, product.toString() + " stored.");
    }

    /**
     * Removes n products from the storage and adds resources to the factory.
     * Removes the oldest products first.
     *
     * @param type type of products to sell
     * @param quantity number of products to sell
     */
    public void sellProduct(ProductType type, int quantity) {
        if (productStorage.containsKey(type)) {
            if (productStorage.get(type).size() >= quantity) {
                for (int i = 0; i < quantity; i++) {
                    productStorage.get(type).remove(0);
                }
                addResources(type, quantity);
            } else {
                throw new IllegalArgumentException("Not enough products of such type stored.");
            }
        } else {
            throw new IllegalArgumentException("No such product stored.");
        }
    }

    /**
     * Checks how many products of certain type are stored.
     *
     * @param type type of the products
     * @return number of the stored products
     */
    public int getProductCount(ProductType type) {
        if (productStorage.containsKey(type)) {
            return productStorage.get(type).size();
        }
        return 0;
    }

    /**
     * Removes every product from the storage, adds resources to the factory.
     */
    public void sellEverything() {
        for (ProductType type : productStorage.keySet()) {
            sellProduct(type, productStorage.get(type).size());
        }
    }

    private void addResources(ProductType type, int quantity) {
        switch (type) {
            case OREO:
                controller.addResources(OREO_PRICE * quantity);
                break;
            case TWIX:
                controller.addResources(TWIX_PRICE * quantity);
                break;
            case BUENO:
                controller.addResources(BUENO_PRICE * quantity);
                break;
            case KITKAT:
                controller.addResources(KITKAT_PRICE * quantity);
                break;
            case BEBE:
                controller.addResources(BEBE_PRICE * quantity);
                break;
        }
    }

}
