package cz.cvut.fel.omo.bendlste.Machines;

import cz.cvut.fel.omo.bendlste.Events.Event;
import cz.cvut.fel.omo.bendlste.FactoryController;
import cz.cvut.fel.omo.bendlste.Product;

import static cz.cvut.fel.omo.bendlste.Utils.MaterialType.*;
import static cz.cvut.fel.omo.bendlste.Utils.Utils.DISPENSER_COST;

public class Dispenser extends Machine {
    public Dispenser(int id, FactoryController controller) {
        super(id, controller);
        controller.useResources(DISPENSER_COST);
    }

    public Product doWork(Product product) {
        switch (product.getType()) {
            case BEBE:
                dispenseMaterialForBebeDough();
                break;
            case OREO:
                dispenseMaterialForOreoDough();
                break;
            case TWIX:
                dispenseMaterialForTwixDough();
                break;
            case BUENO:
                dispenseMaterialForBuenoDough();
                break;
            case KITKAT:
                dispenseMaterialForKitkatDough();
                break;
        }
        return product;
    }

    public void update(Event event) {
        decreaseHealth();
    }

    private void dispenseMaterialForBebeDough() {
        line.getMaterialFromStorage(NUTS, 2);
        line.getMaterialFromStorage(CHOCOLATE, 1);
        line.getMaterialFromStorage(FLOUR, 2);
        line.getMaterialFromStorage(MILK, 3);
    }

    private void dispenseMaterialForOreoDough() {
        line.getMaterialFromStorage(CHOCOLATE, 2);
        line.getMaterialFromStorage(FLOUR, 1);
        line.getMaterialFromStorage(MILK, 5);
        line.getMaterialFromStorage(SUGAR, 1);
    }

    private void dispenseMaterialForTwixDough() {
        line.getMaterialFromStorage(NUTS, 1);
        line.getMaterialFromStorage(CHOCOLATE, 2);
        line.getMaterialFromStorage(FLOUR, 1);
        line.getMaterialFromStorage(MILK, 1);
        line.getMaterialFromStorage(YEAST, 1);
        line.getMaterialFromStorage(CARAMEL, 4);
    }

    private void dispenseMaterialForBuenoDough() {
        line.getMaterialFromStorage(NUTS, 1);
        line.getMaterialFromStorage(CHOCOLATE, 4);
        line.getMaterialFromStorage(FLOUR, 2);
        line.getMaterialFromStorage(MILK, 3);
        line.getMaterialFromStorage(EGGS, 1);
    }

    private void dispenseMaterialForKitkatDough() {
        line.getMaterialFromStorage(NUTS, 1);
        line.getMaterialFromStorage(CHOCOLATE, 2);
        line.getMaterialFromStorage(FLOUR, 2);
        line.getMaterialFromStorage(MILK, 3);
        line.getMaterialFromStorage(YEAST, 1);
        line.getMaterialFromStorage(EGGS, 2);
    }

    @Override
    public String toString() {
        return "Dispenser";
    }
}
