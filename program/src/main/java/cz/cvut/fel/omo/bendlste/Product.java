package cz.cvut.fel.omo.bendlste;

import cz.cvut.fel.omo.bendlste.Utils.MaterialType;
import cz.cvut.fel.omo.bendlste.Utils.ProductType;
import cz.cvut.fel.omo.bendlste.Utils.Shape;

public class Product {
    private ProductType type;
    private MaterialType filling;
    private MaterialType topping;
    private int fragility;
    private int delicacy;
    private int ornamentation;
    private int size;
    private int smoothness;
    private int batch;
    private int positionInLine;
    private Shape shape;
    private boolean packed = false;

    public Product(ProductType type) {
        this.type = type;
        this.positionInLine = 0;
    }

    public int getPositionInLine() {
        return positionInLine;
    }

    public void setPositionInLine(int positionInLine) {
        this.positionInLine = positionInLine;
    }

    public int getBatch() {
        return batch;
    }

    public void setBatch(int batch) {
        this.batch = batch;
    }

    public int getSmoothness() {
        return smoothness;
    }

    public void setSmoothness(int smoothness) {
        this.smoothness = smoothness;
    }

    public Shape getShape() {
        return shape;
    }

    public void setShape(Shape shape) {
        this.shape = shape;
    }

    public ProductType getType() {
        return type;
    }

    public MaterialType getFilling() {
        return filling;
    }

    public MaterialType getTopping() {
        return topping;
    }

    public void setTopping(MaterialType topping) {
        this.topping = topping;
    }

    public void setFilling(MaterialType filling) {
        this.filling = filling;
    }

    public int getFragility() {
        return fragility;
    }

    public void setFragility(int fragility) {
        this.fragility = fragility;
    }

    public int getDelicacy() {
        return delicacy;
    }

    public void setDelicacy(int delicacy) {
        this.delicacy = delicacy;
    }

    public int getOrnamentation() {
        return ornamentation;
    }

    public void setOrnamentation(int ornamentation) {
        this.ornamentation = ornamentation;
    }

    public int getSize() {
        return size;
    }

    public void setSize(int size) {
        this.size = size;
    }

    public boolean isPacked() {
        return packed;
    }

    public void pack() {
        this.packed = true;
    }

    @Override
    public String toString() {
        return "Product{" +
                "type=" + type +
                ", batch=" + batch +
                '}';
    }
}
