package cz.cvut.fel.omo.bendlste.Humans;

import cz.cvut.fel.omo.bendlste.FactoryController;
import cz.cvut.fel.omo.bendlste.Line;
import cz.cvut.fel.omo.bendlste.Visitor;
import cz.cvut.fel.omo.bendlste.Worker;

public class Inspector implements Visitor {
    public Inspector() {
    }

    /**
     * Perform inspector visit of whole factory.
     *
     * @param factoryController controller of the factory to be visited
     */
    public void visitFactory(FactoryController factoryController) {
        factoryController.accept(this);
    }

    /**
     * Perform inspector visit of certain line.
     *
     * @param line line to be visited
     */
    public void visitLine(Line line) {
        line.accept(this);
    }

    /**
     * Perform inspector visit of certain worker.
     *
     * @param worker worker to be visited
     */
    public void visitWorker(Worker worker) {
        worker.accept(this);
    }

}
