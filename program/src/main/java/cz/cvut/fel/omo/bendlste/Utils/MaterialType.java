package cz.cvut.fel.omo.bendlste.Utils;

public enum MaterialType {
    FLOUR,
    SUGAR,
    EGGS,
    YEAST,
    CHOCOLATE,
    MILK,
    CARAMEL,
    NUTS,
    NO_MATERIAL
}
